<%@ page contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="expires" content="-1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PAGOS GOB.mx</title>


    <!-- CSS -->
    <link href="https://framework-gb.cdn.gob.mx/favicon.ico" rel="shortcut icon">
    <link href="https://framework-gb.cdn.gob.mx/assets/styles/main.css" rel="stylesheet">
     <style type="text/css">
     
     BODY { 
     font-family: Open Sans Light; 
     font-size: 18px;
     line-height: 1.428em; 
     }
     
     	.caja{
     	border: 1px solid #545454;
     	border-radius: 6px;
     	padding: 45px 45px 20px;
     	margin-top: 10px;
     	background-color: white;
     	box-shadow: 0px 5px 10px #393C3E, 0 0 0 10px #393C3E inset;
     
     	}
     	.caja label {
     		display: block;
     		font-weight: bold;
     		
     	}
     	.caja div{
     		margin-bottom: 15px;
     		width: 400px;
     	}
     
     </style>
    <!-- Respond.js soporte de media queries para Internet Explorer 8 -->
    <!-- ie8.js EventTarget para cada nodo en Internet Explorer 8 -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/ie8/0.2.2/ie8.js"></script>
    <![endif]-->
   <!--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->
   <!--  <script src="../js/jquery-3.1.0.min.js" type="text/javascript"></script>    -->
   
  </head>
  <body>

    <!-- Contenido -->
    
    <main class="page">
      <div class="container" >
     
        <div id="3ds-prosa" style="width: 500px; margin: auto; ">
      	<div class="alert alert-danger" id="errordatos" style="display: none; margin-top: 10px;">
      	</div>
      	<div style="margin-top: 10px;">Introduzca datos para recibir informaci�n al finalizar el pago</div>
     	 	<div>
     	 	<form method="post" name="form1" id="form1" action="${pageContext.request.contextPath}/pago3DS" class="caja" >
     	 		<input type="hidden" name="idbitacora" id="idbitacora" value="${idbitacora}"/>
     	 		<!--<input type="hidden" name="monto" id="monto" value="${monto}"/> -->
     	 		<!-- <input type="hidden" name="merchant" id="merchant" value="${merchant}"/> -->
     	 		<div class="form-group">
     	 		<label for="nombre">Nombre completo<span class="form-text form-text-error">*</span>:</label>
     	 		<input type="text" name="nombre" id="nombre" class="form-control form-control-error" placeholder="Ingrese su nombre">
     	 		<small class="form-text form-text-error">Campo obligatorio</small>
     	 		</div>
     	 		<div class="form-group">
     	 		<label for="email">Correo<span class="form-text form-text-error">*</span>:</label>
     	 	    <input type="text" name="email" id="email" class="form-control form-control-error" placeholder="ejemplo@dominio.com" />
     	 	    <small class="form-text form-text-error">Campo obligatorio</small>
     	 		</div>
     	 		<div class="form-group">
     	 		<label for="cemail">Confirma Correo<span class="form-text form-text-error">*</span>:</label>
     	 	    <input type="text" name="cemail" id="cemail" class="form-control form-control-error" placeholder="ejemplo@dominio.com"/>
     	 	    <small class="form-text form-text-error">Campo obligatorio</small>
     	 		</div>
     	 		<div class="pull-left text-muted text-vertical-align-button">
                  * Campos obligatorios
                </div>
                <div style="text-align:right">
                	<input type="submit" id="submit" name = "submit" value="Aceptar"  class="btn btn-primary"> 
                </div>                    
        	</form>
        	</div>
        	
	    </div>
      
	    <br><br>
      </div>
    </main>

    <!-- JS -->
    <script src="https://framework-gb.cdn.gob.mx/gobmx.js"></script>
	 <script type="text/javascript">
    $gmx(document).ready(function() {
    	 $('#form1').submit(function() {
    	        // Expresion regular para validar el correo
    	        var regex = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
    	        var reg = /^[a-zA-Z \u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1]{2,45}$/;
    	        if (!reg.test($('#nombre').val()) || $('#email').val().trim() == "" || $('#cemail').val().trim() == "")
    	          {
    	        	  $("#errordatos").show().html("<b>�Error!</b><br/>Favor de introducir los campos obligatorios");
    	        	  return false;
    	          }
    	        
    	        if (!regex.test($('#email').val().trim()) || !regex.test($('#cemail').val().trim())) {
    	        
    	        	$("#errordatos").show().html("<b>�Error!</b><br/>Correo no V�lido");
    	            return false;
    	        }
    	        
    	        if(!regex.test($('#cemail').val().trim()) || $('#cemail').val().trim() !=  $('#email').val().trim())
    	        	{
    	        	    $("#errordatos").show().html("<b>�Error!</b><br/>Los correos no coinciden");
    	        		return false;
    	        	}
    	        
    	        
    	        return true;
    	    });
    	 
    	 
    	 
    	 
    	 
      });
    
    </script>
	
  </body>
</html>
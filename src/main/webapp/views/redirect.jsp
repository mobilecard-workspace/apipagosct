<%-- 
    Document   : comercio_fin
    Created on : 15/03/2016
    Author     : RHT
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
    <head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="Pragma" content="no-cache" />
		<meta http-equiv="expires" content="-1" />
		<meta name="HandheldFriendly" content="true" />
		<meta name="viewport" content="width=device-width" />
		
		<%	
		   	response.setHeader("Expires","0");
		   	response.setHeader("Pragma","no-cache");
		   	response.setDateHeader("Expires",-1);
		 	
		%>
        <title>Procesando</title>
        
         <!-- CSS -->
    	<link href="https://framework-gb.cdn.gob.mx/favicon.ico" rel="shortcut icon">
    	<link href="https://framework-gb.cdn.gob.mx/assets/styles/main.css" rel="stylesheet">
    	
      
        <script type="text/javascript">
            function sendform() {
            	move();
            	document.form1.submit();
            }
            
            function move() {
            	  var elem = document.getElementById("myBar");
            	  var width = 10;
            	  var id = setInterval(frame, 10);
            	  function frame() {
            	    if (width >= 100) {
            	      clearInterval(id);
            	    } else {
            	      width++;
            	      elem.style.width = width + '%';
            	      document.getElementById("label").innerHTML = width * 1  + '%';
            	    }
            	  }
            	}
        </script>
        
        <style>
#myProgress {
  position: relative;
  width: 100%;
  height: 30px;
  background-color: #ddd;
}

#myBar {
  position: absolute;
  width: 10%;
  height: 100%;
  background-color: #4CAF50;
}

#label {
  text-align: center;
  line-height: 30px;
  color: white;
}
</style>

<!-- <style type="text/css">
root {
	display: block;
}

html {
	margin: 0;
	font-size: 62.5%;
	color: white;
	background-color: #D3441C;
}
body {
	font-size: 14px;
    font-weight: bold;
	line-height: 1.428571429;
}

@media ( min-width :750px) {
	.container {
		width: 100%;
		margin-right: auto;
		margin-left: auto;
	}
}

@media ( min-width :992px) {
	.container {
		width: 700px;
		margin-right: auto;
		margin-left: auto;
	}
} 
</style>  -->
    </head>
    <body onload="sendform();">
	
	 <!-- Contenido -->
    <main class="page">
      <div class="container">
      	<form method="post" name="form1" action="http://aplicaciones11.sct.gob.mx/wstest/TestApi">
            <input type="hidden" name="EM_Response" value="${EM_Response}">
            <input type="hidden" name="EM_RefNum" value="${EM_RefNum}">
            <input type="hidden" name="EM_Auth" value="${EM_Auth}">
            <input type="hidden" name="monto" value="${total}">
            <input type="hidden" name="referenciaSCT" value="${referenciaSCT}"> 
            <input type="hidden" name="descripcionSCT" value="${descripcionSCT}">                          
        </form>
        <h2>PROCESANDO PAGO...</h2>
        <div id="myProgress">
		  <div id="myBar">
		    <div id="label">10%</div>
		  </div>
		</div>
        <div class="container">
		<br />
	</div>
      </div>
    </main>

    <!-- JS -->
    <script src="https://framework-gb.cdn.gob.mx/gobmx.js"></script>
    </body>
</html>

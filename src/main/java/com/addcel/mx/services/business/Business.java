package com.addcel.mx.services.business;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.mx.services.model.mapper.ServiceMapper;
import com.addcel.mx.services.model.vo.BitacoraVO;
import com.addcel.mx.services.model.vo.ClienteVO;
import com.addcel.mx.services.model.vo.CorreoVO;
import com.addcel.mx.services.model.vo.Parameters;
import com.addcel.mx.services.utils.Utils;
import com.google.gson.Gson;

public class Business {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Business.class);
	//private DAO _DAO ;
	ServiceMapper mapper;
	
	public Business(ServiceMapper mapper)
	{
		this.mapper = mapper;	
	}
	
	public boolean sendMail(String id_bitacora, String EM_Auth, String EM_RefNum){
		
		boolean success = false;
		try{
		ArrayList<HashMap<String,Object>> result = mapper.getDatosUser(Long.valueOf(id_bitacora));
		HashMap<String,Object> row = result.get(0);
		String Cadena = mapper.getParametro("@MENSAJE_CLIENTES");
		Cadena = Cadena.replace("@NOMBRE", (String)row.get("nombre"));
        Cadena = Cadena.replace("@MONTO", ((BigDecimal)row.get("monto")) +"");
        Cadena = Cadena.replace("@CARGO", ( ((BigDecimal)row.get("monto")).doubleValue() + ((Integer)row.get("comision")).intValue()) + "" );
        Cadena = Cadena.replace("@AUTORIZACION", EM_Auth);
        Cadena = Cadena.replace("@COMISION",((Integer)row.get("comision")).intValue()+".");
        Cadena = Cadena.replace("@REFERENCIA",EM_RefNum);
        java.util.Date fecha = new java.util.Date();
        SimpleDateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Cadena = Cadena.replace("@FECHA", dateFormat.format(fecha));
        String sMail = (String)row.get("email");
        
       // if(Utils.patternMatches(sMail, ".*@(hotmail|live|msn|outlook)\\..*"))
        	SendMailMicrosoft(sMail,"Notificación Pago",Cadena);
       // else
        //	success = Utils.sendMail(sMail, "Notificación Pago" ,Cadena);
        
        LOGGER.debug("Notificacion enviada al user :" + (String)row.get("email") );
        
		}catch(Exception ex){
			LOGGER.error("ERROR AL ENVIAR NOTIFICACION AL USUARIO  " + id_bitacora + " " +  EM_Auth + " Error: " +  ex.getLocalizedMessage());
		}
		
		return success;
		
	}
	
	public boolean SendMailMicrosoft(String sMail, String wAsunto, String Cadena){
    	boolean flag = false;
    	try{
    	
        
        CorreoVO correo = new CorreoVO();
		String from = "no-reply@addcel.com";
		String[]to = {sMail};
		correo.setCc(new String[]{});
		correo.setBody(Cadena);
		correo.setFrom(from);
		correo.setSubject(wAsunto);
		correo.setTo(to);
		Gson gson = new Gson();
		String json = gson.toJson(correo);
		Utils.sendMail(json);
		LOGGER.debug("mensaje hotmail enviado : " + sMail);
    	}catch(Exception ex){
    		LOGGER.error("Ocurrio un error al enviar email user {}:  {}",sMail,ex);
    	}
    	return flag;
    }
	
	
	public static String digest(String text) {
		String digest = "";
		BigInteger bigIntDgst = null;
		
		try {
			LOGGER.debug("DIGEST cadena: " + text);
			MessageDigest md = MessageDigest.getInstance("SHA-1");
			md.update(text.getBytes(), 0, text.length());
			bigIntDgst = new BigInteger(1, md.digest());
			//digest = bigIntDgst.toString(16);
			digest = String.format("%040x", bigIntDgst);
		} catch (NoSuchAlgorithmException e) {
			LOGGER.debug("Error al encriptar - digest", e);
		}
		LOGGER.debug("DIGEST calculado: " + digest);
		return digest;
	}
	
	public  void actualizaBitacora(long id_bitacora, String Nombre, String Email) {
		BitacoraVO bitacora = new BitacoraVO();
		try {
			bitacora.setId_bitacora(id_bitacora);
			bitacora.setNombre(Nombre);
			bitacora.setEmail(Email);
			mapper.actualizaBitacoraDatosUser(bitacora);
		} catch (Exception e) {
			LOGGER.error("OCURRIO UN ERROR AL ACTUALIZAR(Datos usuario) LA TRANSACCION: " + id_bitacora + " --->" +e.getStackTrace());
		}
	}
	
	public  void actualizaBitacora(long id_bitacora, String idAutorizacion, int codigoError, int status, String EM_RefNum, String Card) {
		BitacoraVO bitacora = new BitacoraVO();
		try {
			bitacora.setId_bitacora(id_bitacora);
			bitacora.setCodigo_error(codigoError);
			bitacora.setNo_autorizacion(idAutorizacion);
			bitacora.setStatus(status);
			bitacora.setTicket(" EM_RefNum: " +EM_RefNum + " EM_Auth: " + idAutorizacion);
			bitacora.setTarjeta_compra(Card);
			mapper.actualizaBitacora(bitacora);
		} catch (Exception e) {
			LOGGER.error("OCURRIO UN ERROR AL ACTUALIZAR LA TRANSACCION: " + id_bitacora + " --->" +e.getStackTrace());
		}
	}
	
	public ClienteVO loginClient(String user, String pass){
		try{
			return mapper.getClient(user, pass);
		}catch(Exception ex){
			LOGGER.error("OCURRIO UN ERROR AL OBTENER LOGIN CLIENTE : " + user + " pass: "+ pass + " --->" +ex.getStackTrace());
			ex.printStackTrace();
		}
		return null;
	}
	
	public long insertaTransaccion(ClienteVO client, Parameters parameters) {
		LOGGER.debug("INSERTANDO EN BITACORA LA TRANSACCION PARA EL CLIENTE - ID CLIENTE: " + client.getIdCliente());
		BitacoraVO bitacora = new BitacoraVO(); 
		try {
			bitacora.setCodigo_error(0);
			bitacora.setComision(8);
			bitacora.setConcepto("PAGO " + client.getAlias_organizacion() + " Servicio: " + parameters.getDescripcion());
			bitacora.setExtra(parameters.getOtro());
			bitacora.setFecha(new Date());
			bitacora.setId_bitacora(0);
			bitacora.setId_cliente(client.getIdCliente());
			bitacora.setId_servicio(0);
			bitacora.setMonto(Long.parseLong(parameters.getMonto()));
			bitacora.setNo_autorizacion("");
			bitacora.setReferencia_cliente(parameters.getReferencia());
			bitacora.setStatus(0);
			bitacora.setTarjeta_compra("");
			bitacora.setTicket("PAGO " + client.getAlias_organizacion() + " Servicio: " + parameters.getDescripcion());
			bitacora.setTipo_tarjeta(0);
			bitacora.setNombre("");
			bitacora.setEmail("");
			
			mapper.insertaBitacoraTransaccion(bitacora);
			LOGGER.info(" - ID BITACORA: "+bitacora.getId_bitacora());
			
//			mapper.insertaBitacoraProsa(bitacora);
		} catch (Exception e) {
			LOGGER.error("ERRRO AL GUARDAR LA BITACORA  ");
			e.printStackTrace();
		}
		return bitacora.getId_bitacora();
	}

}

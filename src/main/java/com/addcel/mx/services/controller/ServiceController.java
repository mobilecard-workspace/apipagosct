package com.addcel.mx.services.controller;



import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.addcel.mx.services.business.Business;
import com.addcel.mx.services.model.mapper.ServiceMapper;
import com.addcel.mx.services.model.vo.ClienteVO;
import com.addcel.mx.services.model.vo.CorreoVO;
import com.addcel.mx.services.model.vo.Parameters;
import com.addcel.mx.services.model.vo.ProcomVO;
import com.addcel.mx.services.model.vo.UserVO;
import com.addcel.mx.services.procom.ProcomBusiness;
import com.addcel.mx.services.utils.Constantes;
import com.addcel.mx.services.utils.Utils;
import com.google.gson.Gson;
import com.ironbit.mc.system.crypto.Crypto;

@Controller
public class ServiceController {
	
private static final Logger LOGGER = LoggerFactory.getLogger(ServiceController.class);
	
	@Autowired
	private ServiceMapper mapper;
	
	Parameters parameters =  new Parameters();
	ClienteVO client = null;
	
	@RequestMapping(value="/pago3DS", method=RequestMethod.POST)
	public ModelAndView pago(HttpServletRequest request, Model model){
		ModelAndView mav= null;
		ProcomVO procom = null;
		//procom = new ProcomBusiness().comercioFin(1234567, 1);
		
		
		String email = (String)request.getParameter("email").trim();
		String cemail = (String)request.getParameter("cemail").trim();
		String nombre = (String)request.getParameter("nombre").trim();
		
		String id_bitacora = (String)request.getParameter("idbitacora").trim();
		long monto = Long.parseLong(parameters.getMonto()) + client.getComision();//(String)request.getParameter("monto").trim();
		LOGGER.debug("Monto total: " + monto);
		String merchant = client.getAfiliacion();//(String)request.getParameter("merchant").trim();
		monto = 1;//TODO quitar para produccion
		
		try{
				LOGGER.debug("ID: bitacora: " + id_bitacora);
				LOGGER.debug("MONTO: " + monto);
				
				if(!id_bitacora.isEmpty() && !email.isEmpty() && !nombre.isEmpty() && client != null)
				{
					LOGGER.info("email: ----> " + email);
					new Business(mapper).actualizaBitacora(Long.parseLong(id_bitacora),nombre, email);
					//_DAO.actualizaBitacora(Long.parseLong(id_bitacora),nombre, email);
					procom = new ProcomBusiness().comercioFin(Long.parseLong(id_bitacora), monto,merchant );
					//procom.setDigest(parameters.getReferencia());
					mav = new ModelAndView("comerciofin");//comerciofin
					mav.addObject("prosa", procom);
					mav.addObject("nombre", nombre);
					
					//return new ModelAndView("redirect:" + "https://www.procom.prosa.com.mx/eMerchant/8039159_imdm.jsp");
					//return new ModelAndView("redirect:" + "http://localhost:8080/apiPagoSCT/prueba");
				}else
				{
					mav = new ModelAndView("/");
				}
		}
		catch(Exception ex){
			LOGGER.error("ERROR AL PROCESAR DATOS DEL USUARIO" + ex.getStackTrace());
			ex.printStackTrace();
		}finally{
		
		     return mav;
		}
	}

	/*@RequestMapping(value = "/comercio-con", method = RequestMethod.POST)
	public ModelAndView respuestaProsa(HttpServletRequest request) {
		
		
		 String EM_Response="";
		 String EM_Total="";
		 String EM_OrderID="";
		 String EM_Merchant="";
		 String EM_Store="";
		 String EM_Term="";
		 String EM_RefNum="";
		 String EM_Auth="";
		 String EM_Digest="";
		 @SuppressWarnings("unchecked")
			Map<String, String[]> parametersR = request.getParameterMap();

			for (String key : parametersR.keySet()) {
				this.LOGGER.info(key);

				String[] vals = parametersR.get(key);
				if (key.equals("EM_Response"))
					EM_Response = vals[0];
				else if (key.equals("EM_Total"))
					EM_Total = vals[0];
				else if (key.equals("EM_OrderID"))
					EM_OrderID = vals[0];
				else if (key.equals("EM_Merchant"))
					EM_Merchant = vals[0];
				else if (key.equals("EM_Store"))
					EM_Store = vals[0];
				else if (key.equals("EM_Term"))
					EM_Term = vals[0];
				else if (key.equals("EM_RefNum"))
					EM_RefNum = vals[0];
				else if (key.equals("EM_Auth"))
					EM_Auth = vals[0];
				else if (key.equals("EM_Digest"))
					EM_Digest = vals[0];
				

				for (String val : vals)
					this.LOGGER.info(" -> " + val);
			}
		
		ModelAndView mav= null;
		if(client != null)
		{
			LOGGER.debug("Procesando respuesta prosa EM_Response: "+EM_Response +" EM_Total: "+ EM_Total+" EM_OrderID: "
					+EM_OrderID+" EM_Merchant: "+EM_Merchant+" EM_Store: "+EM_Store+" EM_Term: "+EM_Term+" EM_RefNum: "+ EM_RefNum + " EM_Auth: " + EM_Auth + " EM_Digest:" + EM_Digest);
			
			String digest = Business.digest(EM_Total + EM_OrderID + EM_Merchant + EM_Store + EM_Term + EM_RefNum + "-" +EM_Auth);
			
			if(EM_Response.equals("approved") && digest.equals(EM_Digest)){
				//PAgo exitoso
				new Business(mapper).actualizaBitacora(Long.valueOf(EM_OrderID), EM_Auth, 0, 1,EM_RefNum);
				//_DAO.actualizaBitacora(Long.valueOf(EM_OrderID), EM_Auth, 0, 1); //status = 1 aprovado
				mav = new ModelAndView("redirect");
				mav.addObject("EM_Response", EM_Response);
				mav.addObject("EM_RefNum", EM_RefNum);				
				mav.addObject("EM_Auth", EM_Auth);
				mav.addObject("total", parameters.getMonto());
				mav.addObject("descripcionSCT", parameters.getDescripcion());
				//enviar notificacion email
				//new Business(mapper).sendMail(EM_OrderID, EM_Auth);
				new Business(mapper).sendMail(EM_OrderID, EM_Auth, EM_RefNum);
				LOGGER.debug("PAGO EXITOSO: " + EM_OrderID + " email: "  );
				parameters =  new Parameters();
				client = null;
			}
			else
			{
				//pago no exitoso
				new Business(mapper).actualizaBitacora(Long.valueOf(EM_OrderID), EM_Auth, -1, 2,EM_RefNum);
				//_DAO.actualizaBitacora(Long.valueOf(EM_OrderID), EM_Auth, -1, 2); //status 2  tarjeta rechazada
				mav = new ModelAndView("redirect");
				mav.addObject("EM_Response", EM_Response);
				mav.addObject("EM_RefNum", EM_RefNum);				
				mav.addObject("EM_Auth", EM_Auth);															
				mav.addObject("total", parameters.getMonto());
				mav.addObject("referenciaSCT", parameters.getReferencia());
				mav.addObject("descripcionSCT", parameters.getDescripcion());
				
				//solo para pruebas
				//new Business(mapper).sendMail(EM_OrderID, EM_Auth, EM_RefNum);
				
				LOGGER.debug("PAGO NO EXITOSO: " + EM_OrderID );
			}
		}
		return mav;
		
	}*/
	
	@RequestMapping(value = "/comercio-con", method = RequestMethod.POST)
	public ModelAndView respuestaProsa(@RequestParam String EM_Response,
			@RequestParam String EM_Total, @RequestParam String EM_OrderID,
			@RequestParam String EM_Merchant, @RequestParam String EM_Store,
			@RequestParam String EM_Term, @RequestParam String EM_RefNum,
			@RequestParam String EM_Auth, @RequestParam String EM_Digest, @RequestParam String cc_numberback) {
		
		ModelAndView mav= null;
		if(client != null)
		{
			LOGGER.debug("Procesando respuesta prosa EM_Response: "+EM_Response +" EM_Total: "+ EM_Total+" EM_OrderID: "
					+EM_OrderID+" EM_Merchant: "+EM_Merchant+" EM_Store: "+EM_Store+" EM_Term: "+EM_Term+" EM_RefNum: "+ EM_RefNum + " EM_Auth: " + EM_Auth + " EM_Digest:" + EM_Digest);
			
			String digest = Business.digest(EM_Total + EM_OrderID + EM_Merchant + EM_Store + EM_Term + EM_RefNum + "-" +EM_Auth);
			
			if(EM_Response.equals("approved") && digest.equals(EM_Digest)){
				//PAgo exitoso
				new Business(mapper).actualizaBitacora(Long.valueOf(EM_OrderID), EM_Auth, 0, 1,EM_RefNum,Crypto.aesEncrypt(Utils.parsePass(Constantes.key), cc_numberback));
				//_DAO.actualizaBitacora(Long.valueOf(EM_OrderID), EM_Auth, 0, 1); //status = 1 aprovado
				mav = new ModelAndView("redirect");
				mav.addObject("EM_Response", EM_Response);
				mav.addObject("EM_RefNum", EM_RefNum);				
				mav.addObject("EM_Auth", EM_Auth);
				mav.addObject("total", parameters.getMonto());
				mav.addObject("descripcionSCT", parameters.getDescripcion());
				//enviar notificacion email
				//new Business(mapper).sendMail(EM_OrderID, EM_Auth);
				new Business(mapper).sendMail(EM_OrderID, EM_Auth, EM_RefNum);
				LOGGER.debug("PAGO EXITOSO: " + EM_OrderID + " email: "  );
				parameters =  new Parameters();
				client = null;
			}
			else
			{
				//pago no exitoso
				new Business(mapper).actualizaBitacora(Long.valueOf(EM_OrderID), EM_Auth, -1, 2,EM_RefNum,Crypto.aesEncrypt(Utils.parsePass(Constantes.key), cc_numberback));
				//_DAO.actualizaBitacora(Long.valueOf(EM_OrderID), EM_Auth, -1, 2); //status 2  tarjeta rechazada
				mav = new ModelAndView("redirect");
				mav.addObject("EM_Response", EM_Response);
				mav.addObject("EM_RefNum", EM_RefNum);				
				mav.addObject("EM_Auth", EM_Auth);															
				mav.addObject("total", parameters.getMonto());
				mav.addObject("referenciaSCT", parameters.getReferencia());
				mav.addObject("descripcionSCT", parameters.getDescripcion());
				
				//solo para pruebas
				//new Business(mapper).sendMail(EM_OrderID, EM_Auth, EM_RefNum);
				
				LOGGER.debug("PAGO NO EXITOSO: " + EM_OrderID );
			}
		}
		return mav;
		
	}
	
	/*@RequestMapping(value = "/prueba", method={RequestMethod.GET, RequestMethod.POST} )
	public ModelAndView prueba(HttpServletRequest request, 
	        HttpServletResponse response, RedirectAttributes redirectAttributes){
		ModelAndView mav= null;
			request.setAttribute("parametro1", "Hoola mundo");
			// redirectAttributes.addFlashAttribute("parametro1", "Hoola mundo");
			mav = new ModelAndView("prueba");
			//return new ModelAndView("redirect:" + "prueba.jsp");
			return mav;
	}*/
	
	
	@RequestMapping(value = "/", method=RequestMethod.POST)
	public ModelAndView home(@RequestParam String user, @RequestParam String pass, @RequestParam String referenciaSCT,
			@RequestParam String descripcionSCT, @RequestParam String monto,  @RequestParam String otro ) {	
		LOGGER.debug("Controller");
		
		//_DAO = new DAO(mapper);
		//Parameters parameters = new Parameters();
		parameters.setUser(user.trim());
		parameters.setPass(pass.trim());
		parameters.setReferencia(referenciaSCT.trim());
		parameters.setDescripcion(descripcionSCT.trim());
		parameters.setMonto(monto.trim());
		parameters.setOtro(otro.trim());
		
		
		
		ProcomVO procom = null;
		ModelAndView mav= null; 
		long idBitacora = 0;
		
		client = new Business(mapper).loginClient(parameters.getUser(), parameters.getPass());// _DAO.loginClient("SCT", "123");//mapper.getClient("SCT","123");
		//parameters.setId_cliente(client.getIdCliente());
		
		if(client == null){
			mav = new ModelAndView("error");
		}
		else
			if(client.getEstado() == 0)
			{
				mav = new ModelAndView("no_disponible");
				
			}
			else
			{	
				idBitacora = new Business(mapper).insertaTransaccion(client, parameters);//_DAO.insertaTransaccion(client, parameters);//insertaTransaccion(client, parameters);
				//procom = new ProcomBusiness().comercioFin(idBitacora, monto);
				mav = new ModelAndView("pago");
				mav.addObject("idbitacora", idBitacora);
				//mav.addObject("monto", parameters.getMonto());
				//mav.addObject("merchant", client.getAfiliacion());
				//mav.addObject("prosa", procom);
			}
		
		return mav;	
	}
	
	
	
	@RequestMapping(value = "/test", method=RequestMethod.GET)
	public ModelAndView test(){
		
		return  new ModelAndView("test");
	}
	
	//codigoError 0 aprobada, -1 proceso iniciado,...
	

/*@RequestMapping(value = { "/", "/welcome" }, method = RequestMethod.GET)
public String welcomePage(Model model) {
    model.addAttribute("title", "Welcome");
    model.addAttribute("message", "This is welcome page!");
    return "index";
}	*/

}

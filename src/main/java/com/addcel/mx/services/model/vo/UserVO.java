package com.addcel.mx.services.model.vo;

public class UserVO {
	
	String name;
	String last_name;
	String email;
	
	public UserVO(){
		
	}
	public UserVO(String name, String email){
		this.name = name;
		this.email = email;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	
	

}

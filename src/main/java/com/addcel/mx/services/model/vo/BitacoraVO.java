package com.addcel.mx.services.model.vo;

import java.util.Date;



public class BitacoraVO {

	private long id_bitacora;
	
	private String referencia_cliente;//referencia del cliente
	
	private long id_cliente; 

	private long id_servicio;
	
	private Date fecha;
	
	private String concepto; //descripcion
	
	private String ticket;
	
	private double monto;
	
	private String no_autorizacion;
	
	private int codigo_error;
	
	private int status;
	
	private String tarjeta_compra;
	
	private int tipo_tarjeta;
	
	private int comision;
	
	private String extra;
	
	private String nombre;
	
	private String email;
	

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public long getId_bitacora() {
		return id_bitacora;
	}

	public void setId_bitacora(long id_bitacora) {
		this.id_bitacora = id_bitacora;
	}

	public String getReferencia_cliente() {
		return referencia_cliente;
	}

	public void setReferencia_cliente(String referencia_cliente) {
		this.referencia_cliente = referencia_cliente;
	}

	public long getId_cliente() {
		return id_cliente;
	}

	public void setId_cliente(long id_cliente) {
		this.id_cliente = id_cliente;
	}

	public long getId_servicio() {
		return id_servicio;
	}

	public void setId_servicio(long id_servicio) {
		this.id_servicio = id_servicio;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	public double getMonto() {
		return monto;
	}

	public void setMonto(double monto) {
		this.monto = monto;
	}

	public String getNo_autorizacion() {
		return no_autorizacion;
	}

	public void setNo_autorizacion(String no_autorizacion) {
		this.no_autorizacion = no_autorizacion;
	}

	public int getCodigo_error() {
		return codigo_error;
	}

	public void setCodigo_error(int codigo_error) {
		this.codigo_error = codigo_error;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getTarjeta_compra() {
		return tarjeta_compra;
	}

	public void setTarjeta_compra(String tarjeta_compra) {
		this.tarjeta_compra = tarjeta_compra;
	}

	public int getTipo_tarjeta() {
		return tipo_tarjeta;
	}

	public void setTipo_tarjeta(int tipo_tarjeta) {
		this.tipo_tarjeta = tipo_tarjeta;
	}

	public int getComision() {
		return comision;
	}

	public void setComision(int comision) {
		this.comision = comision;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}
	
	
	
}

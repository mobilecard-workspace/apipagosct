package com.addcel.mx.services.model.mapper;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.ibatis.annotations.Param;

import com.addcel.mx.services.model.vo.BitacoraVO;
import com.addcel.mx.services.model.vo.ClienteVO;

public interface ServiceMapper {

	public int insertaBitacoraTransaccion(BitacoraVO bitacora);
	public void actualizaBitacora(BitacoraVO bitacora);
	public void actualizaBitacoraDatosUser(BitacoraVO bitacora);
	public String getParametro(String parametro);
	public ArrayList<HashMap<String,Object>> getDatosUser(long id_bitacora);
	public ClienteVO getClient(@Param(value = "user") String user, @Param(value = "pass") String pass);
	
}